<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

    
    Route::group(["prefix" => "employe"], function () {
        Route::get("/{id}", "EmployeController@index")->name("employe.index");
        Route::post("/show", "EmployeController@show")->name("employe.show");
    });
    Route::resource('employe', 'EmployeController',['only' => ['store','update','destroy']]);

    Route::group(["prefix" => "contract"], function () {
        Route::get("/{id}", "ContractController@index")->name("contract.index");
        Route::post("/show", "ContractController@show")->name("contract.show");
        Route::post("/update", "ContractController@update")->name("contract.update");
    });
    Route::resource('contract', 'ContractController',['only' => ['store','destroy']]);

    Route::group(["prefix" => "type"], function () {
        Route::get("/{id}", "TypeController@index")->name("type.index");
        Route::post("/show", "TypeController@show")->name("type.show");
    });
    Route::resource('type', 'TypeController',['only' => ['store','update','destroy']]);

    Route::group(["prefix" => "children"], function () {
        Route::get("/{id}", "ChildrenController@index")->name("children.index");
        Route::post("/show", "ChildrenController@show")->name("children.show");
    });
    Route::resource('children', 'ChildrenController',['only' => ['store','update','destroy']]);
