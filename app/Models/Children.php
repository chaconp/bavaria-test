<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Children extends Model
{
    protected $guarded = ['id'];
    use SoftDeletes;
    protected $perPage = 10;

    public function employe() {
        return $this->belongsTo(Employe::class, "employe_id");
    }

}
