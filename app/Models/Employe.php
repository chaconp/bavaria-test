<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Employe extends Model
{
    protected $guarded = ['id'];
    use SoftDeletes;
    protected $perPage = 10;

    public function type() {
        return $this->belongsTo(Type::class, "type_id");
    }

}
