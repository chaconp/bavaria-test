<?php

namespace App\Services;

use Illuminate\Http\Request;
use App\Models\Type;
use App\Http\Resources\Type as TypeRes;


class TypeService
{
    public function __construct()
    {
    }
    public function index(int $id)
    {

        try {
            $type = Type::findOrFail($id);
            return new TypeRes($type);
        } catch (\Exception $exception) {
            return response()->json(["message" => __(":app: Record not found !!", ["app" => env('APP_NAME')])]);
        }
    }
    public function show(Request $request)
    {
        try {

            $filter = $request->filter;
            //Permite aplicar el filtro a varias columnas
            $type = Type::where(function ($query) use ($filter) {
                $query->where('name', 'like', '%' . $filter . '%');
            })
                ->paginate($request->per_page);

            return TypeRes::collection($type);
        } catch (\Exception $exception) {
            return response()->json(["error" => json_encode($exception)]);
        }
    }

    public function store(Request $request)
    {
        try {

            $type = new Type();
            $type->name = $request->name;
            $type->save();

            return new TypeRes($type);
        } catch (\Exception $exception) {
            return response()->json(["error" => json_encode($exception)]);
        }
    }
    public function update(Request $request, int $id)
    {
        try {
            $type = Type::findOrFail($id);


            $type->name = $request->name;
            $type->save();


            return new TypeRes($type);
        } catch (\Exception $exception) {
            return response()->json(["message" => __(":app: Record not found !!", ["app" => env('APP_NAME')])]);
        }
    }
    public function destroy(int $id)
    {
        try {
            $type = Type::findOrFail($id);
            $type->delete();

            return response()->json(["message" => __(":app : The record has been deleted !!", ["app" => env('APP_NAME')])]);
        } catch (\Exception $exception) {
            return response()->json(["message" => __(":app: Record not found !!", ["app" => env('APP_NAME')])]);
        }
    }

}
