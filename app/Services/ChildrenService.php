<?php

namespace App\Services;

use Illuminate\Http\Request;
use App\Models\Children;
use App\Http\Resources\Children as ChildrenRes;


class ChildrenService
{
    public function __construct()
    {
    }
    public function index(int $id)
    {

        try {
            $children = Children::findOrFail($id);
            return new ChildrenRes($children);
        } catch (\Exception $exception) {
            return response()->json(["message" => __(":app: Record not found !!", ["app" => env('APP_NAME')])]);
        }
    }
    public function show(Request $request)
    {
        try {

            $filter = $request->filter;
            $employe_id = $request->employe_id;
            //Permite aplicar el filtro a varias columnas
            $children = Children::where(function ($query) use ($filter) {
                $query->where('name', 'like', '%' . $filter . '%');
            })
            //Si el filtro employe_id es null, no se aplica este al query
                ->when($request->employe_id, function ($query) use ($employe_id) {
                    return $query->where('employe_id', $employe_id);
                })
                ->with("employe")
                ->paginate($request->per_page);

            return ChildrenRes::collection($children);
        } catch (\Exception $exception) {
            return response()->json(["error" => json_encode($exception)]);
        }
    }

    public function store(Request $request)
    {
        try {

            $children = new Children();
            $children->name = $request->name;
            $children->age = $request->age;
            $children->employe_id = $request->employe_id;
            $children->save();

            return new ChildrenRes($children);
        } catch (\Exception $exception) {
            return response()->json(["error" => json_encode($exception)]);
        }
    }
    public function update(Request $request, int $id)
    {
        try {
            $children = Children::findOrFail($id);


            $children->name = $request->name;
            $children->age = $request->age;
            $children->employe_id = $request->employe_id;

            $children->save();


            return new ChildrenRes($children);
        } catch (\Exception $exception) {
            return response()->json(["message" => __(":app: Record not found !!", ["app" => env('APP_NAME')])]);
        }
    }
    public function destroy(int $id)
    {
        try {
            $children = Children::findOrFail($id);
            $children->delete();

            return response()->json(["message" => __(":app : The record has been deleted !!", ["app" => env('APP_NAME')])]);
        } catch (\Exception $exception) {
            return response()->json(["message" => __(":app: Record not found !!", ["app" => env('APP_NAME')])]);
        }
    }

}
