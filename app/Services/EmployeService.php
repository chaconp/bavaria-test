<?php

namespace App\Services;

use Illuminate\Http\Request;
use App\Models\Employe;
use App\Http\Resources\Employe as EmployeRes;


class EmployeService
{
    public function __construct()
    {
    }
    public function index(int $id)
    {

        try {
            $employe = Employe::findOrFail($id);
            return new EmployeRes($employe);
        } catch (\Exception $exception) {
            return response()->json(["message" => __(":app: Record not found !!", ["app" => env('APP_NAME')])]);
        }
    }
    public function show(Request $request)
    {
        try {

            $filter = $request->filter;
            $type_id = $request->type_id;
            //Permite aplicar el filtro a varias columnas
            $employe = Employe::where(function ($query) use ($filter) {
                $query->where('name', 'like', '%' . $filter . '%')
                    ->orWhere('phone', 'like', '%' . $filter . '%')
                    ->orWhere('address', 'like', '%' . $filter . '%');
            })
            //Si el filtro type_id es null, no se aplica este al query
                ->when($request->type_id, function ($query) use ($type_id) {
                    return $query->where('type_id', $type_id);
                })
                ->with("type")
                ->paginate($request->per_page);

            return EmployeRes::collection($employe);
        } catch (\Exception $exception) {
            return response()->json(["error" => json_encode($exception)]);
        }
    }

    public function store(Request $request)
    {
        try {

            $employe = new Employe();
            $employe->name = $request->name;
            $employe->phone = $request->phone;
            $employe->address = $request->address;
            $employe->type_id = $request->type_id;
            $employe->save();

            return new EmployeRes($employe);
        } catch (\Exception $exception) {
            return response()->json(["error" => json_encode($exception)]);
        }
    }
    public function update(Request $request, int $id)
    {
        try {
            $employe = Employe::findOrFail($id);


            $employe->name = $request->name;
            $employe->phone = $request->phone;
            $employe->address = $request->address;
            $employe->type_id = $request->type_id;
 
            $employe->save();


            return new EmployeRes($employe);
        } catch (\Exception $exception) {
            return response()->json(["message" => __(":app: Record not found !!", ["app" => env('APP_NAME')])]);
        }
    }
    public function destroy(int $id)
    {
        try {
            $employe = Employe::findOrFail($id);
            $employe->delete();

            return response()->json(["message" => __(":app : The record has been deleted !!", ["app" => env('APP_NAME')])]);
        } catch (\Exception $exception) {
            return response()->json(["message" => __(":app: Record not found !!", ["app" => env('APP_NAME')])]);
        }
    }

}
