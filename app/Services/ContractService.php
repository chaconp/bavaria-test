<?php

namespace App\Services;

use Illuminate\Http\Request;
use App\Models\Contract;
use App\Http\Resources\Contract as ContractRes;


class ContractService
{
    public function __construct()
    {
    }
    public function index(int $id)
    {

        try {
            $contract = Contract::findOrFail($id);
            return new ContractRes($contract);
        } catch (\Exception $exception) {
            return response()->json(["message" => __(":app: Record not found !!", ["app" => env('APP_NAME')])]);
        }
    }
    public function show(Request $request)
    {
        try {

            $filter = $request->filter;
            $employe_id = $request->employe_id;
            //Permite aplicar el filtro a varias columnas
            $contract = Contract::where(function ($query) use ($filter) {
                $query->where('name', 'like', '%' . $filter . '%');
            })
            //Si el filtro type_id es null, no se aplica este al query
                ->when($request->employe_id, function ($query) use ($employe_id) {
                    return $query->where('employe_id', $employe_id);
                })
                ->with("employe")
                ->paginate($request->per_page);

            return ContractRes::collection($contract);
        } catch (\Exception $exception) {
            return response()->json(["error" => json_encode($exception)]);
        }
    }

    public function store(Request $request)
    {
        try {

            $contract = new Contract();
            $contract->name = $request->name;
            $contract->date =   $request->date;
            $contract->file = '';
            $contract->employe_id = $request->employe_id;
            $contract->save();

            if ($request->hasFile('file')) {
                $file = $request->file('file');
                $extension = $file->getClientOriginalExtension();
                $fileName = 'contract_' . $contract->id . '.' . $extension;
                $path = storage_path('app/public/file/contract/');
                //error_log('ruta:: ' . $path);
                $file->move($path, $fileName);

                $contract->file =  'app/public/file/contract/' . $fileName;
                $contract->save();
            }


            return new ContractRes($contract);
        } catch (\Exception $exception) {
            return response()->json(["error" => json_encode($exception)]);
        }
    }
    public function update(Request $request)
    {
        try {
            $contract = Contract::findOrFail($request->id);


            $contract->name = $request->name;
            $contract->date = $request->date;
            $contract->file = $request->file;
            $contract->employe_id = $request->employe_id;
 
            if ($request->hasFile('file')) {
                $file = $request->file('file');
                $extension = $file->getClientOriginalExtension();
                $fileName = 'contract_' . $contract->id . '.' . $extension;
                $path = storage_path('app/public/file/contract/');
                $file->move($path, $fileName);
                $contract->file =  'app/public/file/contract/' . $fileName;
            }

            $contract->save();


            return new ContractRes($contract);
        } catch (\Exception $exception) {
            return response()->json(["error" => json_encode($exception)]);
            //return response()->json(["message" => __(":app: Record not found !!", ["app" => env('APP_NAME')])]);
        }
    }
    public function destroy(int $id)
    {
        try {
            $contract = Contract::findOrFail($id);
            $contract->delete();

            return response()->json(["message" => __(":app : The record has been deleted !!", ["app" => env('APP_NAME')])]);
        } catch (\Exception $exception) {
            return response()->json(["message" => __(":app: Record not found !!", ["app" => env('APP_NAME')])]);
        }
    }

}
