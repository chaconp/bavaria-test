<?php

namespace App\Http\Controllers;

use App\Models\Children;
use Illuminate\Http\Request;
use App\Services\ChildrenService;
use Illuminate\Support\Facades\Log;

class ChildrenController extends Controller
{
    

    public function __construct(ChildrenService $childrenService)
    {
        $this->childrenService = $childrenService;
    }
    /**
     * Permite consultar registro por su id.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     * retorna en la respuesta el objeto en formato json
     */
    public function index(int $id)
    {
        if (request()->ajax()) {
            if (request()->isMethod("GET")) {
                return $this->childrenService->index($id);
            }
        }
        abort(401);        
    }
    
    /**
     * Permite consultar una lista de registros aplicando una seri de filtros especializado.
     *
     * @param  \Illuminate\Http\Request  $request
     * Rertorna un objeto paginador con el resultado de la busqueda, este objeto en formato json
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        if (request()->ajax()) {
            if (request()->isMethod("POST")) {
                return $this->childrenService->show($request);
            }
        }
        abort(401);

    }

    /**
     * Permite la creación de un registro, recibe en el reques el objeto con sus atributos.
     *
     * @param  \Illuminate\Http\Request  $request
     * Retorna en formato json el objeto creado con su id
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (request()->ajax()) {
            if (request()->isMethod("POST")) {
                return $this->childrenService->store($request);
            }
        }
        abort(401);
        
    }

     /**
     * Actualiza el objeto con el identificador en viado "id". y los datos en el request
     *
     * @param  \Illuminate\Http\Request  $request
     * Retorna el objeto modificado en formato json
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, int $id)
    {
        if (request()->ajax()) {
            if (request()->isMethod("PUT")) {
                return $this->childrenService->update($request,$id);
            }
        }
        abort(401);        
    }

    /**
     * Elimina el registro lógico, recibe en employe el id del registro a eliminar.
     *
      * @param  \Illuminate\Http\Request  $request
     * Retorna un mensaje que indica que el registro se ha eliminado.
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, int $id)
    {
        if (request()->ajax()) {
            if (request()->isMethod("DELETE")) {
                return $this->childrenService->destroy($id);
            }
        }
        abort(401);
    }
}
