<?php

namespace App\Http\Controllers;

use App\Models\Contract;
use Illuminate\Http\Request;
use App\Services\ContractService;
use Illuminate\Support\Facades\Log;

class ContractController extends Controller
{
    

    public function __construct(ContractService $contractService)
    {
        $this->contractService = $contractService;
    }
    /**
     * Permite consultar registro por su id.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     * retorna en la respuesta el objeto en formato json
     */
    public function index(int $id)
    {
        if (request()->ajax()) {
            if (request()->isMethod("GET")) {
                return $this->contractService->index($id);
            }
        }
        abort(401);        
    }
    
    /**
     * Permite consultar una lista de registros aplicando una seri de filtros especializado.
     *
     * @param  \Illuminate\Http\Request  $request
     * Rertorna un objeto paginador con el resultado de la busqueda, este objeto en formato json
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        if (request()->ajax()) {
            if (request()->isMethod("POST")) {
                return $this->contractService->show($request);
            }
        }
        abort(401);

    }

    /**
     * Permite la creación de un registro, recibe en el reques el objeto con sus atributos.
     *
     * @param  \Illuminate\Http\Request  $request
     * Retorna en formato json el objeto creado con su id
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (request()->ajax()) {
            if (request()->isMethod("POST")) {
                return $this->contractService->store($request);
            }
        }
        abort(401);
        
    }

     /**
     * Actualiza el objeto con el identificador en viado "id". y los datos en el request
     *
     * @param  \Illuminate\Http\Request  $request
     * Retorna el objeto modificado en formato json
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        if (request()->ajax()) {
            if (request()->isMethod("POST")) {
                return $this->contractService->update($request);
            }
        }
        abort(401);        
    }

    /**
     * Elimina el registro lógico, recibe en contract el id del registro a eliminar.
     *
     * @param  \App\Models\Contract  $contract
     * Retorna un mensaje que indica que el registro se ha eliminado.
     * @return \Illuminate\Http\Response
     */
    public function destroy(Contract $contract)
    {
        if (request()->ajax()) {
            if (request()->isMethod("DELETE")) {
                return $this->contractService->destroy($contract->id);
            }
        }
        abort(401);
    }
}
