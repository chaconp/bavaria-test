<?php

namespace App\Http\Controllers;

use App\Models\Type;
use Illuminate\Http\Request;
use App\Services\TypeService;
use Illuminate\Support\Facades\Log;

class TypeController extends Controller
{
    

    public function __construct(TypeService $typeService)
    {
        $this->typeService = $typeService;
    }
    /**
     * Permite consultar registro por su id.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     * retorna en la respuesta el objeto en formato json
     */
    public function index(int $id)
    {
        if (request()->ajax()) {
            if (request()->isMethod("GET")) {
                return $this->typeService->index($id);
            }
        }
        abort(401);        
    }
    
    /**
     * Permite consultar una lista de registros aplicando una seri de filtros especializado.
     *
     * @param  \Illuminate\Http\Request  $request
     * Rertorna un objeto paginador con el resultado de la busqueda, este objeto en formato json
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        if (request()->ajax()) {
            if (request()->isMethod("POST")) {
                return $this->typeService->show($request);
            }
        }
        abort(401);

    }

    /**
     * Permite la creación de un registro, recibe en el reques el objeto con sus atributos.
     *
     * @param  \Illuminate\Http\Request  $request
     * Retorna en formato json el objeto creado con su id
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (request()->ajax()) {
            if (request()->isMethod("POST")) {
                return $this->typeService->store($request);
            }
        }
        abort(401);
        
    }

     /**
     * Actualiza el objeto con el identificador en viado "id". y los datos en el request
     *
     * @param  \Illuminate\Http\Request  $request
     * Retorna el objeto modificado en formato json
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, int $id)
    {
        if (request()->ajax()) {
            if (request()->isMethod("PUT")) {
                return $this->typeService->update($request,$id);
            }
        }
        abort(401);        
    }

    /**
     * Elimina el registro lógico, recibe en type el id del registro a eliminar.
     *
     * @param  \App\Models\Type  $type
     * Retorna un mensaje que indica que el registro se ha eliminado.
     * @return \Illuminate\Http\Response
     */
    public function destroy(Type $type)
    {
        if (request()->ajax()) {
            if (request()->isMethod("DELETE")) {
                return $this->typeService->destroy($type->id);
            }
        }
        abort(401);
    }
}
